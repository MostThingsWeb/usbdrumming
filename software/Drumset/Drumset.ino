#include <SPI.h>

const int PIN_SS = 2;
const int PIN_MISO = 14;
const int PIN_CLK = 15;
const int PIN_MOSI = 16;

const int PIN_RED_LED = 10;
const int PIN_GREEN_LED = 5;
const int PIN_BLUE_LED = 9;

const int PIN_OE = 3;
const int PIN_FAULT = 4;

// Define some colors
const int RED[] = {
  255, 0, 0};
const int GREEN[] = {
  0, 255, 0};
const int BLUE[] = {
  0, 0, 255};
const int ORANGE[] = {
  255, 128, 0};
const int YELLOW[] = {
  255, 255, 0};
const int WHITE[] = {
  255, 255, 255}; // For the pedal

void setup(){
  pinMode(PIN_SS, OUTPUT);

  pinMode(PIN_FAULT, INPUT);
  pinMode(PIN_OE, OUTPUT);
  digitalWrite(PIN_OE, HIGH);

  // Initialize SPI
  SPI.setDataMode(SPI_MODE1);
  SPI.setClockDivider(SPI_CLOCK_DIV8);
  SPI.begin();

  // Set up RBGB led
  pinMode(PIN_RED_LED, OUTPUT);
  pinMode(PIN_GREEN_LED, OUTPUT);
  pinMode(PIN_BLUE_LED, OUTPUT);



  Serial.begin(9600);

  // Show the startup LED pattern
  for (int i = 0; i <= 255; i++){
    setLED(i, 0, 255 - i);
    delay(3);
  }
  for (int i = 0; i <= 255; i++){
    setLED(255 - i, i, 0);
    delay(3);
  }
  for (int i = 0; i <= 255; i++){
    setLED(0, 255 - i, i);
    delay(3);

  }

  setLED(0, 0, 0);

  // Inititalize drumset
  digitalWrite(PIN_SS, LOW);
  delayMicroseconds(27.5);
  SPI.transfer(0xAA);
  delayMicroseconds(8);
  digitalWrite(PIN_SS, HIGH);
  delayMicroseconds(10004.5);

}

bool fault = false;

void loop(){
  if (fault){
    Serial.println("{ \"msgType\": \"error\", \"error\": \"Drumset is on!\" }");
    delay(2000);
    return;
  }

  if (digitalRead(PIN_FAULT) == HIGH){
    fault = true;
    SPI.end();
    // Release the buffer enable line, isolating
    // the Pro Micro from everything else
    digitalWrite(PIN_OE, LOW);
    return;
  }

  // The client might want to know what we are, so let
  // them ping us
  if (Serial.available()){
    while (Serial.available()){
      Serial.read();
    }

    Serial.println("{ \"msgType\": \"ping\" }");
  }


  setLED(0, 0, 0);

  // Select the slave
  digitalWrite(PIN_SS, LOW);
  delayMicroseconds(27.5);

  // Send preamble
  SPI.transfer(0xAA);
  delayMicroseconds(32.625);

  // Read the number of messages that the drumset has to send
  byte hitCount = SPI.transfer(0x55);
  byte pad;
  byte velocity;

  if (hitCount > 0){ 
    delayMicroseconds(50);

    // All messages are encoded as JSON
    Serial.print("{ \"msgType\": \"hits\", \"hitCount\": ");
    Serial.print(hitCount);
    Serial.print(", \"hits\": [");

    for (int hit = 0; hit < hitCount; hit++){
      SPI.transfer(0); // Returns 153 (0x99)
      delayMicroseconds(23.125);
      pad = SPI.transfer(0);
      delayMicroseconds(23.125);
      velocity = SPI.transfer(0);
      delayMicroseconds(23.125);

      Serial.print("{\"pad\": ");
      Serial.print(pad);
      Serial.print(", \"veloc\": ");
      Serial.print(velocity);
      Serial.print("}");
      if (hit != hitCount - 1){
        Serial.print(",");
      }
    }

    Serial.println("]}");
  } 

  delayMicroseconds(48);
  digitalWrite(PIN_SS, HIGH);

  if (hitCount == 1){
    switch (pad){
    case 0x26: 
      setLEDColor(RED);
      break;
    case 0x2E:
      setLEDColor(YELLOW);
      break;
    case 0x30:
      setLEDColor(BLUE);
      break;
    case 0x31:
      setLEDColor(ORANGE);
      break;
    case 0x2D:
      setLEDColor(GREEN);
      break;
    case 0x24:
      setLEDColor(WHITE);
      break;
    } 

    delay(5);
  } 
  else if (hitCount > 1){
    setLEDColor(WHITE);
    delay(5);
  }
}

void setLED(int r, int g, int b){
  analogWrite(PIN_RED_LED, r);
  analogWrite(PIN_GREEN_LED, g);
  analogWrite(PIN_BLUE_LED, b);
}

void setLEDColor(const int* color){
  setLED(color[0], color[1], color[2]);
}


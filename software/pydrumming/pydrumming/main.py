import argparse
import colorama
from colorama import Fore, Back, Style # Fabulous library
import glob
import json
from os.path import normpath, join, dirname
import platform
import pygame
import serial
import signal
import sys

# Stores the directory of the wav files
wav_directory = ""

# From http://stacyprowell.com/blog/2009/03/30/trapping-ctrlc-in-python/
class BreakHandler:
    '''
    Trap CTRL-C, set a flag, and keep going.  This is very useful for
    gracefully exiting database loops while simulating transactions.
 
    To use this, make an instance and then enable it.  You can check
    whether a break was trapped using the trapped property.
 
    # Create and enable a break handler.
    ih = BreakHandler()
    ih.enable()
    for x in big_set:
        complex_operation_1()
        complex_operation_2()
        complex_operation_3()
        # Check whether there was a break.
        if ih.trapped:
            # Stop the loop.
            break
    ih.disable()
    # Back to usual operation...
    '''
 
    def __init__(self, emphatic=9):
        '''
        Create a new break handler.
 
        @param emphatic: This is the number of times that the user must
                    press break to *disable* the handler.  If you press
                    break this number of times, the handler is automagically
                    disabled, and one more break will trigger an old
                    style keyboard interrupt.  The default is nine.  This
                    is a Good Idea, since if you happen to lose your
                    connection to the handler you can *still* disable it.
        '''
        self._count = 0
        self._enabled = False
        self._emphatic = emphatic
        self._oldhandler = None
        return
 
    def _reset(self):
        '''
        Reset the trapped status and count.  You should not need to use this
        directly; instead you can disable the handler and then re-enable it.
        This is better, in case someone presses CTRL-C during this operation.
        '''
        self._count = 0
        return
 
    def enable(self):
        '''
        Enable trapping of the break.  This action also resets the
        handler count and trapped properties.
        '''
        if not self._enabled:
            self._reset()
            self._enabled = True
            self._oldhandler = signal.signal(signal.SIGINT, self)
        return
 
    def disable(self):
        '''
        Disable trapping the break.  You can check whether a break
        was trapped using the count and trapped properties.
        '''
        if self._enabled:
            self._enabled = False
            signal.signal(signal.SIGINT, self._oldhandler)
            self._oldhandler = None
        return
 
    def __call__(self, signame, sf):
        '''
        An break just occurred.  Save information about it and keep
        going.
        '''
        self._count += 1
        # If we've exceeded the "emphatic" count disable this handler.
        if self._count >= self._emphatic:
            self.disable()
        return
 
    def __del__(self):
        '''
        Python is reclaiming this object, so make sure we are disabled.
        '''
        self.disable()
        return
 
    @property
    def count(self):
        '''
        The number of breaks trapped.
        '''
        return self._count
 
    @property
    def trapped(self):
        '''
        Whether a break was trapped.
        '''
        return self._count > 0

def main():
    global wav_directory
    
    colorama.init()
    
    """parser = argparse.ArgumentParser(description='Interface via serial connection to a Guitar Hero World Tour drumset.')
    parser.add_argument('integers', metavar='N', type=int, nargs='+',
                   help='an integer for the accumulator')

    args = parser.parse_args()
    """
    
    # Print banner
    print Style.BRIGHT
    print "              |                              "
    print "    " + Fore.BLUE + "|=\\" + Fore.YELLOW + " | |" + Fore.RESET + " /=| /= | | /=\=\ /=\=\ = /=\ /=| "
    print "    " + Fore.BLUE + "|=/" +  Fore.YELLOW + " \=| " + Fore.RESET + "\=| |  \=/ | | | | | | | | | \=| "
    print "    " + Fore.BLUE + "|" +    Fore.YELLOW+"   \=|" + Fore.RESET +"                              \=| "
    print Style.RESET_ALL + ""
    print "      v1.0, by Chris Laplante (mostthingsweb.com)"
    print ""
    print ""    
    
    write("Initializing audio...       ")

    # Find the location of our wav file directory
    wav_directory = normpath(join(dirname(__file__), "resources/wavs"))
    
    # Initialize the mixer
    
    # IMPORTANT NOTE!: If the drum sounds are laggy or don't sound right, then
    # play with the frequency and buffer parameters below. These are what seem to
    # work for me on my laptop. If sounds just aren't playing, try changing the
    # set_num_channels number from 16 to something higher
    
    pygame.mixer.init(frequency=44100, size=-16, channels=8, buffer=1028)
    pygame.mixer.set_num_channels(16)
    pygame.init()
    
    write_done()
    write("Scanning for drumset...     ")
    
    ports = list_serial_ports()
    ports_len = len(ports)
    
    if ports_len == 0:
        write(Fore.RED + Style.BRIGHT)
        writeln("Failed!")
        print ""
        print "Error: No serial ports detected. Please make sure the drumset is plugged in. If you have just plugged in the drumset, wait a few seconds before trying again. If it still doesn't work, then unplug and then replug it in."
        print Fore.RESET + Style.RESET_ALL
        enter_quit()
    
    found_drumset = False
    drumset_port_id = None
    
    # Scan ports for drumsets
    for port_id in ports:
        port = configure_port(port_id)
        # Add a 5 second timeout so we aren't waiting forever for some serial-interfaced 
        # toaster to respond
        port.timeout = 5
        port.open()
        if test_port(port):
            found_drumset = True
            drumset_port_id = port_id
    
    if found_drumset:
        writeln(Fore.GREEN + Style.BRIGHT + "Found on port '" + str(drumset_port_id) + "'")
        write(Fore.RESET + Style.RESET_ALL)
    else:
        write(Fore.RED + Style.BRIGHT)
        writeln("Failed!")
        print ""
        print "Error: One or more serial devices were detected, but did not respond to the ping. Please plug in a drumset and try again."
        print Fore.RESET + Style.RESET_ALL
        enter_quit()
    
    write("Preparing to play...        ")
    
    port = configure_port(drumset_port_id)
    # The only reason there is a delay is so that Ctrl + C can be caught
    port.timeout = 5
    port.open()
    
    write_done()
    print ""
    
    # Pad mappings (the 'pad' property) for JSON responses
    #
    # These correspond to MIDI Percussion instruments by default, though the mappings can be customized
    # through command line parameters
    #
    # Orange hihat = 49
    # Yellow hihat = 46
    # Green drum   = 45
    # Blue drum    = 48
    # Red drum     = 38
    # Pedal        = 36
    
    print Style.BRIGHT + Fore.GREEN + "Ok, go ahead and play!" + Fore.RESET
    print ""
    print Fore.YELLOW + "## To quit, press Ctrl + C (may take a few seconds to quit and cleanup). ##" + Fore.RESET + Style.RESET_ALL
    
    # Only one of each of the hihats can be playing at a time, so store their sound objects
    orange_hihat = None
    yellow_hihat = None
    
    bh = BreakHandler()
    bh.enable()
    
    while True:
        response = None
        
        try:
            response = json.loads(port.readline())
        except:
            # Timeout; check for Ctrl + C
            if bh.trapped:
                bh.disable()
                break
            continue
        
        for hit in response["hits"]:
            instrument = hit["pad"]

            if instrument == 49:
                # Orange hihat hit
                if orange_hihat is not None:
                    orange_hihat.stop()
                orange_hihat = pygame.mixer.Sound(instrument_path(49))
                orange_hihat.play()
            elif instrument == 46:
                # Yellow hihat hit
                if yellow_hihat is not None:
                    yellow_hihat.stop()
                yellow_hihat = pygame.mixer.Sound(instrument_path(46))
                yellow_hihat.play()
            else:
                # Some other pad/pedal hit
                sound = pygame.mixer.Sound(instrument_path(instrument))
                sound.play()
    
    port.close()
    
# A function that tries to list serial ports on most common platforms
# http://stackoverflow.com/q/11303850/221061
def list_serial_ports():
    system_name = platform.system()
    if system_name == "Windows":
        # Scan for available ports. return a list of tuples (num, name)
        available = []
        for i in range(256):
            try:
                s = serial.Serial(i)
                available.append(i)
                s.close()
            except serial.SerialException:
                pass
        return available
    elif system_name == "Darwin":
        # Mac
        return glob.glob('/dev/tty*') + glob.glob('/dev/cu*')
    else:
        # Assume Linux or something else
        return glob.glob('/dev/ttyS*') + glob.glob('/dev/ttyUSB*')
        
    
    
# Below are some more utility methods

# Configure and return a port to use with the Pro Micro
def configure_port(port_id):
    ser = serial.Serial()
    ser.port = port_id
    ser.baudrate = 9600
    ser.rtscts = True
    ser.dsrdtr = True
    return ser

# Test to see if a port is the drumset, by using the ping command
def test_port(port):
    port.write("ping")
    port.flush()
    
    try:
        response = port.readline()
        j = json.loads(response)
        msgType = j["msgType"]
        if msgType == "ping":
            return True
    except:
        return False
        
    return False

# Write text without newline
def write(string):
    sys.stdout.write(string)
    
# Write text with newline
def writeln(string):
    sys.stdout.write(string + "\n")
    
def write_done():
    writeln(Fore.GREEN + Style.BRIGHT + "Done!" + Fore.RESET + Style.RESET_ALL)
    
def enter_quit():
    raw_input(" Press Enter to quit...")
    sys.exit(0)
        
# Returns the path to the wav file given a MIDI Percussion number
def instrument_path(number):
    global wav_directory
    return join(wav_directory, "i" + str(number) + ".wav")
    
if __name__ == '__main__':
    main()

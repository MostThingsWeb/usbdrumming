#!/usr/bin/env python

from setuptools import setup
from platform import system
from sys import exit

# Detect windows
windows = system() == "Windows"

# Detect pygame
pygame = True
try:
    import pygame
except:
    pygame = False

# If pygame isn't installed, and we are running on windows, quit
if windows and (not pygame):
    print ""
    print "Warning! Pygame is not installed. On Windows, you should use the pygame installer (binary) instead of relying on setuptools."
    print "Please visit http://www.pygame.org/download.shtml to download and install before running this installer again."
    print ""
    raw_input("Press Enter to exit...")
    exit(0)

setup(name='pydrumming',
        version='1.0',
        description='Python client for serial interface to Guitar Hero World Tour drumsets',
        author='Chris Laplante',
        url='http://www.mostthingsweb.com',
        packages=['pydrumming'],
        install_requires=['pyserial', 'pygame', 'colorama'], 
        entry_points = {
            'console_scripts': [
                'pydrumming = pydrumming.main:main'
            ]
        },
        package_data = {
            '': ['resources/wavs/*.wav']
        }
     )

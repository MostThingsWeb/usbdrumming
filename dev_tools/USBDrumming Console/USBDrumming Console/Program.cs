﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Media;
using System.Threading;
using System.Web.Script.Serialization;
using Midi;
using USBDrumming;

namespace USBDrumming_Console {
    class Program {
        static bool a = false;

        static void Main(string[] args) {
            Console.WriteLine("Started!");
            Console.WriteLine();


            // Find the right port
            var ports = SerialPort.GetPortNames();

            if (ports.Length == 0) {
                Console.WriteLine("No ports");
                Thread.Sleep(3000);
                Environment.Exit(0);
            }

            var port = new SerialPort("COM8", 9600);
            port.RtsEnable = true;
            port.DtrEnable = true;
            port.Open();

            JavaScriptSerializer j = new JavaScriptSerializer();
            j.RegisterConverters(new JavaScriptConverter[] { new DynamicJsonConverter() });

            Console.WriteLine("Pinging...");
            port.WriteLine("ping");
            Console.WriteLine(port.ReadLine());
            Console.WriteLine("Ping sent");

            //  Prompt the user to choose an output device (or if there is only one, use that one).
            OutputDevice outputDevice = ExampleUtil.ChooseOutputDeviceFromConsole();
            if (outputDevice == null) {
                Console.WriteLine("No output devices, so can't run this example.");
                ExampleUtil.PressAnyKeyToContinue();
                return;
            }


            outputDevice.Open();

            // Velocity goes from 1 to 126
            int velocity = 100;

            int pedal = (int)Percussion.BassDrum1;
            int red = (int)Percussion.ClosedHiHat;
            int blue = (int)Percussion.SnareDrum1;
            int green = (int)Percussion.MidTom1;
            int orange = (int)Percussion.RideCymbal1;
            int yellow = (int)Percussion.CrashCymbal1;

            SoundPlayer bassDrum = new SoundPlayer(@"C:\instruments\i36.wav");
            SoundPlayer closedHiHat = new SoundPlayer(@"C:\instruments\i42.wav");
            SoundPlayer snare = new SoundPlayer(@"C:\instruments\i38.wav");
            SoundPlayer midTom = new SoundPlayer(@"C:\instruments\i47.wav");
            SoundPlayer rideCymbal = new SoundPlayer(@"C:\instruments\i51.wav");
            SoundPlayer crash = new SoundPlayer(@"C:\instruments\i49.wav");

            closedHiHat.Load();
            snare.Load();
            midTom.Load();
            rideCymbal.Load();
            crash.Load();
            bassDrum.Load();

            while (true) {
                var a = port.ReadLine();
                dynamic json = j.Deserialize(a, typeof(object)) as dynamic;

                Dictionary<string, object> c = json.hits[0];

                // var b = json.hits[0];
                Console.WriteLine(c["pad"]);
                switch ((int)c["pad"]) {
                    case 45:
                        midTom.Play();
                        break;
                    case 48:
                        snare.Play();
                        break;
                    case 36:
                        bassDrum.Play();
                        break;
                    case 38:
                        closedHiHat.Play();
                        break;
                    case 49:
                        rideCymbal.Play();
                        break;
                    case 46:
                        crash.Play();
                        break;


                }

            }

            Console.ReadLine();
        }

    }

    public class ExampleUtil {
        /// <summary>
        /// Chooses an output device, possibly prompting the user at the console.
        /// </summary>
        /// <returns>The chosen output device, or null if none could be chosen.</returns>
        /// If there is exactly one output device, that one is chosen without prompting the user.
        public static OutputDevice ChooseOutputDeviceFromConsole() {
            if (OutputDevice.InstalledDevices.Count == 0) {
                return null;
            }
            if (OutputDevice.InstalledDevices.Count == 1) {
                return OutputDevice.InstalledDevices[0];
            }
            Console.WriteLine("Output Devices:");
            for (int i = 0; i < OutputDevice.InstalledDevices.Count; ++i) {
                Console.WriteLine("   {0}: {1}", i, OutputDevice.InstalledDevices[i].Name);
            }
            Console.Write("Choose the id of an output device...");
            while (true) {
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                int deviceId = (int)keyInfo.Key - (int)ConsoleKey.D0;
                if (deviceId >= 0 && deviceId < OutputDevice.InstalledDevices.Count) {
                    return OutputDevice.InstalledDevices[deviceId];
                }
            }
        }

        /// <summary>
        /// Chooses an input device, possibly prompting the user at the console.
        /// </summary>
        /// <returns>The chosen input device, or null if none could be chosen.</returns>
        /// If there is exactly one input device, that one is chosen without prompting the user.
        public static InputDevice ChooseInputDeviceFromConsole() {
            if (InputDevice.InstalledDevices.Count == 0) {
                return null;
            }
            if (InputDevice.InstalledDevices.Count == 1) {
                return InputDevice.InstalledDevices[0];
            }
            Console.WriteLine("Input Devices:");
            for (int i = 0; i < InputDevice.InstalledDevices.Count; ++i) {
                Console.WriteLine("   {0}: {1}", i, InputDevice.InstalledDevices[i]);
            }
            Console.Write("Choose the id of an input device...");
            while (true) {
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                int deviceId = (int)keyInfo.Key - (int)ConsoleKey.D0;
                if (deviceId >= 0 && deviceId < InputDevice.InstalledDevices.Count) {
                    return InputDevice.InstalledDevices[deviceId];
                }
            }
        }

        /// <summary>
        /// Prints "Press any key to continue." with a newline, then waits for a key to be pressed.
        /// </summary>
        public static void PressAnyKeyToContinue() {
            Console.WriteLine("Press any key to continue.");
            Console.ReadKey(true);
        }

        /// <summary>
        /// Key mappings for mock MIDI keys on the QWERTY keyboard.
        /// </summary>
        private static Dictionary<ConsoleKey, int> mockKeys = new Dictionary<ConsoleKey, int>
        {
            {ConsoleKey.Q,        53},
            {ConsoleKey.D2,       54},
            {ConsoleKey.W,        55},
            {ConsoleKey.D3,       56},
            {ConsoleKey.E,        57},
            {ConsoleKey.D4,       58},
            {ConsoleKey.R,        59},
            {ConsoleKey.T,        60},
            {ConsoleKey.D6,       61},
            {ConsoleKey.Y,        62},
            {ConsoleKey.D7,       63},
            {ConsoleKey.U,        64},
            {ConsoleKey.I,        65},
            {ConsoleKey.D9,       66},
            {ConsoleKey.O,        67},
            {ConsoleKey.D0,       68},
            {ConsoleKey.P,        69},
            {ConsoleKey.OemMinus, 70},
            {ConsoleKey.Oem4,     71},
            {ConsoleKey.Oem6,     72}
        };

        /// <summary>
        /// If the specified key is one of the computer keys used for mock MIDI input, returns true
        /// and sets pitch to the value.
        /// </summary>
        /// <param name="key">The computer key pressed.</param>
        /// <param name="pitch">The pitch it mocks.</param>
        /// <returns></returns>
        public static bool IsMockPitch(ConsoleKey key, out Pitch pitch) {
            if (mockKeys.ContainsKey(key)) {
                pitch = (Pitch)mockKeys[key];
                return true;
            }
            pitch = 0;
            return false;
        }
    }
}

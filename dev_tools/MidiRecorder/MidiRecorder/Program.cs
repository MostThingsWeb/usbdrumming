﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sanford;
using Sanford.Multimedia.Midi;
using System.Threading;
using Sanford.Multimedia.Timers;
using iTunesLib;

namespace MidiRecorder {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("Writing instrument files...");

            var iTunes = new iTunesLib.iTunesAppClass();

            for (int instrumentNum = 35; instrumentNum < 82; instrumentNum++) {
                if (instrumentNum == 49 || instrumentNum == 52 || instrumentNum == 55 || instrumentNum == 57) {
                    continue;
                }

                using (Sequence sequence = new Sequence()) {
                    Track track1 = new Track();
                    sequence.Add(track1);

                    ChannelMessageBuilder builder = new ChannelMessageBuilder();

                    builder.Command = ChannelCommand.NoteOn;
                    builder.MidiChannel = 9;
                    builder.Data1 = instrumentNum;
                    builder.Data2 = 127;
                    builder.Build();

                    track1.Insert(0, builder.Result);

                    builder.Command = ChannelCommand.NoteOff;
                    builder.Data2 = 0;
                    builder.Build();

                    track1.Insert(60, builder.Result);

                    sequence.Save(String.Format("c:\\instruments\\i{0}.mid", instrumentNum));

                }

                Console.WriteLine("Adding file...");
                var newFile = iTunes.LibraryPlaylist.AddFile(@"C:\instruments\i" + instrumentNum.ToString() + ".mid");

                Console.WriteLine("Waiting...");
                while (newFile.InProgress) { }

                foreach (IITTrack newTrack in newFile.Tracks) {

                    var convertedTrack = iTunes.ConvertTrack(newTrack);
                    while (convertedTrack.InProgress) { }

                    foreach (IITTrack t1 in convertedTrack.Tracks) {
                        newTrack.Delete();
                    }
                    Console.WriteLine("Done");
                }
            }

        }
    }
}
